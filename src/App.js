import React, { Suspense } from 'react';
import { ThemeProvider, Button } from '@material-ui/core';
import { Provider } from 'react-redux';

import './App.css';
import { theme } from './config';
import store from './store';
import Header from './Header';
import CreateInvestor from './Investor/CreateInvestor';

function App() {
  return (
    <Provider store={store}>
      <ThemeProvider theme={theme}>
        <Header />
        <CreateInvestor />
      </ThemeProvider>
    </Provider>
  );
}

export default App;
