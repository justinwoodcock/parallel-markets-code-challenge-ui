import { createMuiTheme } from '@material-ui/core/styles';

export const themeColor = 'rgb(5, 36, 69)';

export const theme = createMuiTheme({
  palette: {
    primary: {
      main: themeColor,
      contrastText: '#fff'
    },
  },
  typography: {
    // Use the system font instead of the default Roboto font.
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
    fontSize: 12,
    useNextVariants: true,
  },
});
