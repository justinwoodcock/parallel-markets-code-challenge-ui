import { init } from '@rematch/core';

import investor from './Investor/actions';

const models = {
  investor,
};

const store = init({
  models,
  plugins: [],
});

export default store;
