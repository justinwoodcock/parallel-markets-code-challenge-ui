import React from 'react';
import {
  AppBar,
  Toolbar,
  Slide,
  useScrollTrigger
} from '@material-ui/core';
import styled from 'styled-components'

import logo from './logo.png';

const ButtonWrapper = styled.div`
  button {
    margin-right: 8px;
  }
`;

function HideOnScroll(props) {
  const { children, window } = props;
  const trigger = useScrollTrigger({ target: window ? window() : undefined });

  return (
    <Slide appear={false} direction="down" in={!trigger}>
      {children}
    </Slide>
  );
}

const Header = props => {

  return (
    <HideOnScroll {...props}>
      <AppBar
        position="fixed"
        color="primary">
        <Toolbar>
          <img src={logo}
            alt="logo"
            style={{
              height: 32,
              marginRight: 6,
            }} />
        </Toolbar>
      </AppBar>
    </HideOnScroll>
  )
}

Header.propTypes = {

}

export default Header
