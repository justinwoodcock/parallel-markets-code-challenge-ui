const API_URL = 'https://parallel-markets-challenge-api.herokuapp.com';
const INVESTOR_PATH = '/investors';
const UPLOAD_PATH = '/upload';

export default {
  state: {
    investor: {},
    files: [],
  },

  reducers: {
    setInvestor: (state, investor) => ({
      ...state,
      investor,
    }),
  },

  effects: dispatch => ({
    async create(formData) {
      let investorFiles = [];
      const {
        firstName,
        lastName,
        streetAddress,
        state,
        zipCode,
        phoneNumber,
        dateOfBirth,
        files,
      } = formData;

      if (files && files.length > 0) {
        investorFiles = await this.upload(formData.files);
      }

      const newInvestorData = {
        firstName,
        lastName,
        streetAddress,
        state,
        zipCode,
        phoneNumber,
        dateOfBirth,
        files: investorFiles,
      }
      
      return new Promise(async (resolve, reject) => {

        return fetch(`${API_URL}${INVESTOR_PATH}`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(newInvestorData),
        })
          .then(res => res.json())
          .then(data => {
            this.setInvestor(data);
            resolve(data);
          })
          .catch(err => reject(err));

      }).catch((err) => { throw err.message; });

    },

    async upload(files) {
      let formData = new FormData();
      await files.forEach(file => formData.append('files', file, file.name));

      return new Promise(async (resolve, reject) => {

        return fetch(`${API_URL}${UPLOAD_PATH}`, {
          method: 'POST',
          body: formData,
        })
          .then(res => res.json())
          .then(data => {
            return resolve(data);
          })
          .catch(err => reject(err));

      }).catch((err) => { throw err.message; });
    },

  })
}