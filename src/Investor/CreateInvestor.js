import React, { useState } from 'react';
import styled from 'styled-components';
import {
  Card,
  CardContent,
  Typography,
  TextField,
  CardActions,
  Button,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow
} from '@material-ui/core';
import { Controller, useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import MuiPhoneNumber from 'material-ui-phone-number';
import MomentUtils from '@date-io/moment';
import {
  MuiPickersUtilsProvider,
  DatePicker,
} from '@material-ui/pickers';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import UploadFiles from './UploadFiles';

const CreateInvestorWrapper = styled.div`
  display: flex;
  flex-direction: column;
    height: 100%;
    justify-content: center;
`;

const CreateInvestorForm = styled.form`
  display: flex;
  width: 80%;
  align-self: center;

  @media (min-width: 1080px) {
    width: 60%;
  }

  @media (min-width: 1080px) {
    width: 40%;
  }

  > div {
    width: 100%;
  }

  .columns {
    display: flex;
    
    > div {
      display: flex;
      flex: 1;
      margin-right: 20px;

      &:last-child {
        margin-right: 0;
      }
    }
  }

`;

function CreateInvestor(props) {
  const dispatch = useDispatch();
  const { register, handleSubmit, control, reset, setValue, errors } = useForm({
    mode: 'onBlur',
  });
  const [ dateOfBirth, setDateOfBirth ] = useState(null);
  const [ phoneNumber, setPhoneNumber ] = useState(null);
  const [ files, setFiles ] = useState(null);

  console.log('errors', errors);

  const onSubmit = async (data) => {
    const newInvestor = {
      ...data,
      dateOfBirth,
      phoneNumber,
      files,
    }

    dispatch.investor.create(newInvestor).then(res => {
      console.log('res', res);
    });
  };

  return (
    <CreateInvestorWrapper>
      <CreateInvestorForm onSubmit={handleSubmit(onSubmit)}>
        <Card>
          <CardContent>
            <Typography gutterBottom variant="h5" component="h1">
              Create Investor
            </Typography>
            <Typography variant="body2" color="textSecondary" component="p">
              Some info about creating an Investor here...
            </Typography>
            <div className="columns">
              <Controller
                name="firstName"
                as={<TextField />}
                control={control}
                defaultValue=""
                fullWidth
                label="First Name"
                placeholder="The first name of the investor"
                margin="dense"
                rules={{ required: true }}
                error={errors.firstName} />
              <Controller
                name="lastName"
                as={<TextField />}
                control={control}
                defaultValue=""
                fullWidth
                label="Last Name"
                placeholder="The last name of the investor"
                margin="dense"
                rules={{ required: true }}
                error={errors.lastName} />
            </div>
            <Controller
              name="streetAddress"
              as={<TextField />}
              control={control}
              defaultValue=""
              fullWidth
              label="Street Address"
              placeholder="The street address of the investor"
              margin="dense"
              rules={{ required: true }}
              error={errors.streetAddress} />
            <div className="columns">
              <Controller
                name="state"
                as={<TextField />}
                control={control}
                defaultValue=""
                fullWidth
                label="State"
                placeholder="The state of the investor"
                margin="dense"
                rules={{ required: true }}
                error={errors.state} />
              <Controller
                name="zipCode"
                as={<TextField />}
                control={control}
                defaultValue=""
                fullWidth
                label="Zip Code"
                placeholder="The zip code of the investor"
                margin="dense"
                rules={{ required: true }}
                error={errors.zipCode} />
            </div>

            <div className="columns">
              <MuiPhoneNumber
                label="Phone Number"
                fullWidth
                margin="dense"
                defaultCountry={'us'}
                onChange={phoneNumber => setPhoneNumber(phoneNumber)} />

              <MuiPickersUtilsProvider utils={MomentUtils}>
                <DatePicker
                  name="dateOfBirth"
                  variant="inline"
                  format="MM/DD/yyyy"
                  margin="dense"
                  id="dateOfBirth"
                  label="Date of Birth"
                  value={dateOfBirth}
                  onChange={val => setDateOfBirth(val)}
                  fullWidth
                />
              </MuiPickersUtilsProvider>
            </div>

            <UploadFiles onAddFiles={setFiles} />

            {
              !files ? null : (
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>Filename</TableCell>
                      <TableCell>Type</TableCell>
                      <TableCell>Size</TableCell>
                      <TableCell>Actions</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {
                      files.map((file, i) => (
                        <TableRow key={i}>
                          <TableCell>
                            { file.name }
                          </TableCell>
                          <TableCell>
                            { file.type }
                          </TableCell>
                          <TableCell>
                            { file.size }
                          </TableCell>
                          <TableCell>
                            <DeleteForeverIcon />
                          </TableCell>
                        </TableRow>
                      ))
                    }
                  </TableBody>
                </Table>
              )
            }
          </CardContent>
          <CardActions>
            <Button variant="contained" color="primary" type="submit">
              Submit
            </Button>
            <Button variant="outlined" color="primary" onClick={reset}>
              Cancel
            </Button>
          </CardActions>
        </Card>
      </CreateInvestorForm>
    </CreateInvestorWrapper>
  )
}

export default CreateInvestor
