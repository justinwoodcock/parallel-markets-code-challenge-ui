import React, {useCallback} from 'react'
import PropTypes from 'prop-types'
import {useDropzone} from 'react-dropzone'
import styled from 'styled-components';

import { themeColor } from '../config';

const UploadWrapper = styled.div`
  border: none;
    width: calc(100% - 80px);
    height: 160px;
    display: flex;
    justify-content: center;
    flex-direction: column;
    align-items: center;
    background-color: ${themeColor};
    outline: 2px dashed #fff;
    outline-offset: -10px;
    -webkit-transition: outline-offset .15s ease-in-out, background-color .15s linear;
    transition: outline-offset .15s ease-in-out, background-color .15s linear;
    border-radius: 3px;
    color: #fff;
    padding: 0 40px;
    margin-top: 20px;
    text-align: center;

  .upload-info {
    flex: 1;
    display: flex;
    justify-content: center;

    p {
      display: flex;
      align-items: center;
    }
  }

  &.is-active {
    background-color: #2d5a89;
    outline-offset: -15px;
  }

`;

const UploadFiles = props => {
  const onDrop = useCallback(async files => props.onAddFiles(files), [])
  const {getRootProps, getInputProps, isDragActive} = useDropzone({onDrop});

  return (
    <UploadWrapper {...getRootProps()}>
      <input {...getInputProps()} />
      <div className="upload-info">
      {
        isDragActive ?
          <p>Drop your file here ...</p> :
          <p>Drag 'n' drop your file here, or click to select a file</p>
      }
      </div>
    </UploadWrapper>
  )
}

UploadFiles.propTypes = {
  onAddFiles: PropTypes.func.isRequired,
}

export default UploadFiles
